// --release — убрать sourcemaps, добавить баннер, сжать js и релизный include файлов

let showAddInfo = false;

/*===================================
=            Dependences            =
===================================*/

var sourcemaps = require('gulp-sourcemaps'),
	runSequence = require('run-sequence'), // запускает таски в определеном порядке
	fileinclude = require('gulp-file-include'),
	livereload = require('gulp-livereload'),
	cleanCSS = require('gulp-clean-css'), // оптимизирует css
	postcss = require('gulp-postcss'),
	connect = require('gulp-connect'), // отображение в локалхост
	plumber = require('gulp-plumber'), // debugger
	rename = require('gulp-rename'),
	gulpif = require('gulp-if'),
	useref = require('gulp-useref'), // соединяет файлы в html (build:)
	notify = require('gulp-notify'),
	insert = require('gulp-insert'), // вставляет текст в файл
	colors = require('colors'),
	gutil = require('gulp-util'),
	babel = require('gulp-babel'),
	watch = require('gulp-watch'),
	clean = require('gulp-clean'), // удаляет папочки :)
	argv = require('yargs').argv, // обрабатывает флаги запуска
	gulp = require('gulp');

var autoprefixer = require('autoprefixer'),
	conditionals = require('postcss-conditionals'), // условия
	smartImport = require('postcss-smart-import'), // import компонентов
	comments = require('postcss-discard-comments'), // удаляет комменты
	clearfix = require('postcss-clearfix'),
	flexbugs = require('postcss-flexbugs-fixes'),
	reporter = require('postcss-reporter'), // errors log
	cssnext = require('postcss-cssnext'), // возможности css4
	nested = require('postcss-nested'), // вложенность
	mixins = require('postcss-mixins'), // миксины
	extend = require('postcss-simple-extend'), // наследование стилей scss
	focus = require('postcss-focus'), // всем ховерам добавляет фокус
	color = require('postcss-color-function'), // scss цветовые функции
	short = require('postcss-short'), // позволяет писать сокращенно
	scss = require('postcss-scss'), // синтаксис scss
	vars = require('postcss-simple-vars'); // переменные

/*=====  End of Dependences  ======*/


var path = {
	dist: { //Тут мы укажем куда складывать готовые после сборки файлы
		html: 'dist/',
		js: 'dist/js/',
		css: 'dist/css/'
	},
	src: { //Пути откуда брать исходники
		html: 'src/*.html', //Синтаксис src/*.html говорит gulp что мы хотим взять все файлы с расширением .html
		js: 'src/js/**/*.js', //В стилях и скриптах нам понадобятся только main файлы
		css: 'src/css/**/*.scss'
	},
	watch: { //Тут мы укажем, за изменением каких файлов мы хотим наблюдать
		html: 'src/**/*.html',
		js: 'src/js/**/*.js',
		css: 'src/css/**/*.*'
	},
	clean: 'dist'
};

var banner = [
	'',
	'/*',
	'',
	'Hello Friend! I wish you pleasant work with my code. :)',
	'front-end:',
	'		Pavel Dreit',
	'		www.dreit.ru',
	'',
	'*/',
	'',
	''
].join('\n');

var processors = [
	smartImport({ 
		root: "src/css",
		path: ["../css", "../css/components", "../css/data"]
	}),
	comments({ removeAll: true }),
	autoprefixer(),
	short(),
	mixins(),
	nested(),
	extend(),
	vars(),
	conditionals(),
	clearfix(),
	focus(),
	color(),
	flexbugs,
	reporter()
];

var notifyTemplate = notify.onError({
						title: '<%= error.plugin %>',
						// message: '<%= error.fileName %>'
				})

/*===========================
=            CSS            =
===========================*/

gulp.task('css', function() {
	return gulp.src(path.src.css)
		.pipe(plumber({
			errorHandler: notifyTemplate
		}))
		.pipe( gulpif(!argv.release, sourcemaps.init() ) )
		.pipe(sourcemaps.init())
		.pipe(postcss(processors), {syntax: scss})
		.pipe( gulpif(argv.release, insert.prepend(banner) ) )
		.pipe(rename({ extname: ".css" }))
		.pipe( gulpif(!argv.release, sourcemaps.write() ) )
		.pipe(gulp.dest(path.dist.css))
		.pipe(connect.reload());
});

/*=====  End of CSS  ======*/


/*============================
=            HTML            =
============================*/

gulp.task('html', function() {
	let stream = gulp.src(path.src.html) //Выберем файлы по нужному пути

	stream
		.pipe(plumber({
			errorHandler: notifyTemplate
		}))
		.pipe(fileinclude({
			prefix: '.@',
			basepath: '@file',
			context: {
				env: argv.release ? "prod" : "dev"
			}
		}))
		.pipe(useref())

	stream
		.pipe(gulp.dest(path.dist.html)) //Выплюнем их в папку dist
		.pipe(connect.reload());

	return stream;
});

/*=====  End of HTML  ======*/


gulp.task('js', function() {
	return gulp.src(path.src.js) //Найдем наш main файл
		.pipe(plumber({
			errorHandler: notifyTemplate
		}))
		.pipe( gulpif(!argv.release, sourcemaps.init() ) ) //Инициализируем sourcemap
		.pipe(fileinclude({
			prefix: '.@',
			basepath: '@file',
			context: {
				env: argv.release ? "prod" : "dev"
			}
		}))
		.pipe(babel({
				presets: ['env']
		}))
		.pipe(rename({ extname: ".js" }))
		.pipe( gulpif(argv.release, insert.prepend(banner) ) )
		.pipe( gulpif(!argv.release, sourcemaps.write() ) ) //Пропишем карты
		.pipe(gulp.dest(path.dist.js)) //Выплюнем готовый файл в dist
		.pipe(connect.reload());
});


gulp.task('clean-dist', function() {
	return gulp.src(path.clean, { read: false })
		.pipe(plumber())
		.pipe(clean())
});

gulp.task('copy-src-root', function(callback) {
	return gulp.src(['src/*.txt', 'src/*.htaccess', 'src/*.php', 'src/*.json', 'src/other/**/*'])
		.pipe(plumber())
		.pipe(gulp.dest('dist/'));
});



/*=============================
=            Watch            =
=============================*/

gulp.task('watch', function() {

	var watchTasks = {};
	var watchDelay = 200;
	var watchTasksIsSet = {};

	let watcherDelayer = (watcher, type)=>{
		if (!watchTasksIsSet[type]) {return;}
		watcher.close();
		watchTasksIsSet[type] = false;
		setTimeout(()=>{
			watchTasksIsSet[type] = true;
			watchTasks[type]();
		}, watchDelay);
	}

	var execsEndTimers = {};

	let execsEndListener = (type, cb)=>{

		clearTimeout(execsEndTimers[type]);

		execsEndTimers[type] = setTimeout(()=>{
			cb();
		}, watchDelay + 100);

	};


	watchTasks.html = ()=>{
		let watcher = watch([path.watch.html], (event, cb)=>{
			watcherDelayer(watcher, 'html');
			execsEndListener('html', ()=>{
				gulp.start('html');
				console.log("changed", "html".cyan);
			});
		});
	};
	watchTasks.css = ()=>{
		let watcher = watch([path.watch.css], (event, cb)=>{
			watcherDelayer(watcher, 'css');
			execsEndListener('css', ()=>{
				gulp.start('css');
				console.log("changed", "css".cyan);
			});
		});
	};
	watchTasks.js = ()=>{
		let watcher = watch([path.watch.js], (event, cb)=>{
			watcherDelayer(watcher, 'js');
			execsEndListener('js', ()=>{
				gulp.start('js');
				gulp.start('html');
				console.log("changed", "js".cyan);
			});
		});
	};

	for (prop in watchTasks) {
		if (watchTasks.hasOwnProperty(prop)) {
			watchTasksIsSet[prop] = true;
			watchTasks[prop]();
		}
	}
});

/*=====  End of Watch  ======*/


/*====================================
=            gulp connect            =
====================================*/

var net = require('net');
function getNetworkIP(callback) {
	var socket = net.createConnection(80, 'www.google.com');
	socket.on('connect', function() {
		callback(undefined, socket.address().address);
		socket.end();
	});
	socket.on('error', function(e) {
		callback(e, 'error');
	});
}

gulp.task('connect', function() {
	getNetworkIP(function (error, ip) {
			if (error) {
				console.log('error:'.red, error);
				ip = '127.0.0.1';
				console.log('Used Local IP:');
			} else {
				console.log('Used External IP:');
			}
			console.log(ip);
			connect.server({
				port: 4444,
				host: ip,
				root: './dist',
				https: false,
				livereload: {start: true}
			});
	});
});

/*=====  End of gulp connect  ======*/


gulp.task('build', function(callback) {
	argv.release = true;
	return runSequence('clean-dist', ['js', 'css', 'copy-src-root', 'html'], 'html');
});

gulp.task('default', function(argument) {
	return runSequence('clean-dist', ['js', 'html', 'copy-src-root'], 'connect', 'watch');
});
