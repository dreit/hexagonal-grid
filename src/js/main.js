.@include("polyfills.js");
.@include("_base.js");
.@include("ui-elements.js");

function noop() {}

var colors = ["LimeGreen", "Crimson", "Chocolate", "Brown", "MediumVioletRed", "DarkRed", "OrangeRed", "Teal", "Cyan", "Indigo", "Fuchsia", "DodgerBlue"]
var emptyGroups = Array.from(colors);
var groups = {};


/*===============================
=            HEXAGON            =
===============================*/

class Hexagon {
	constructor (coord, opts = {}) {
		this.element = stringToHTML('<span class="hex"></span>').children[0];
		this.coord = coord;
		this.group = null;

		this.isHidden = opts.hasOwnProperty('isHidden') ? opts.isHidden : true;

		if (this.isHidden) {
			this.hide();
		}

		this.setColor(opts.color);

		const setClickEvent = ()=>{
			this.element.addEventListener('click', (e)=>{
				this.activate();
			}, false);
		}
		setClickEvent();

	}

	activate () {
		this.lookAround();
		this.defineSurroundGroups();
		this.joinGroup();
		document.dispatchEvent(new Event('onChangeGrid'));
	}

	lookAround () {
		const getAllCoords = ()=>{
			this.surround = [
				[this.coord[0]-1, this.coord[1]-1 + ((this.coord[0]-1+gridIsEven) % 2)],
				[this.coord[0]-1, this.coord[1] + ((this.coord[0]-1+gridIsEven) % 2)],

				[this.coord[0], this.coord[1]-1],
				[this.coord[0], this.coord[1]+1],

				[this.coord[0]+1, this.coord[1]-1 + ((this.coord[0]+1+gridIsEven) % 2)],
				[this.coord[0]+1, this.coord[1] + ((this.coord[0]+1+gridIsEven) % 2)],

			];
		}
		getAllCoords();
		const checkSurround = ()=>{
			var result = [];
			for (var i = 0; i < this.surround.length; i++) {
				if (this.surround[i][0] < 0 || this.surround[i][1] < 0 || this.surround[i][0] > fullHeight -1 || this.surround[i][1] > fullWidth -1) {
					
				} else {
					if (!hiddingMaskMap[this.surround[i][0]][this.surround[i][1]]) {
						result.push(this.surround[i]);
					}
				}
			}
			this.surround = result;
		}
		checkSurround();
		// this.markSurround();
	}

	defineSurroundGroups () {
		var surroundGroups = [];
		var biggestGroup = '';
		for (var i = 0; i < this.surround.length; i++) {
			let group = hexCells[this.surround[i][0]][this.surround[i][1]].getGroupName();
			if (group) {surroundGroups.push(group)}
		}

		var groupsAndSizez = [];
		var groupSizes = [];

		for (var i = 0; i < surroundGroups.length; i++) {
			let prop = surroundGroups[i];
			groupsAndSizez.push([prop, window.groups[prop].length]);
			groupSizes.push(window.groups[prop].length);
		}

		function compareRandom(a, b) {
			return Math.random() - 0.5;
		}
		groupsAndSizez.sort(compareRandom);

		var highestVal = Math.min.apply(null, groupSizes) == Infinity ? 0 : Math.min.apply(null, groupSizes);
		for (var i = 0; i < groupsAndSizez.length; i++) {
			if (groupsAndSizez[i][1] == highestVal) {
				biggestGroup = groupsAndSizez[i][0];
				break;
			}
		}
		this.biggestSurroundGroup = biggestGroup;
		this.surroundGroups = surroundGroups;
	}

	markSurround () {
		for (var i = 0; i < this.surround.length; i++) {
			hexCells[this.surround[i][0]][this.surround[i][1]].joinGroup('black');
		}
	}

	joinGroup (groupName) {
		const createGroup = (group)=>{
			if (group && !window.groups.hasOwnProperty(group)) {
				return window.groups[group] = [];
			}
		}
		if (this.groupName) {
			return;
		}
		const getGroupName = ()=>{
			let position = Math.floor(Math.random() * (window.emptyGroups.length-1));
			let color = window.emptyGroups[position];
			window.emptyGroups.splice(position, 1);
			createGroup(color);
			return color;
		}
		const getGroup = ()=>{
			if (groupName && !window.groups[groupName]) {
				createGroup(groupName);
				console.log('Added custom group: ', groupName);
			}
			groupName = groupName || this.biggestSurroundGroup || this.groupName || getGroupName();
			if (!groupName) {
				console.log('Dont have a group');
				return;
			}
			if (!this.group) {
				this.group = createGroup(groupName);
				this.groupName = groupName;

				window.groups[groupName].push(this);
			}
		}
		getGroup();
		this.setColor(this.groupName);
	}

	setColor (color = '#ccc') {
		this.color = color;
		this.element.style.color = color;
	}

	getCode () {
		return this.element;
	}

	getGroupName () {
		return this.groupName;
	}

	getGroup () {
		return this.group;
	}

	hide () {
		this.element.classList.add('hidden');
		this.isHidden = true;
	}
}


/*=====  End of HEXAGON  ======*/


// ()=>{


/*======================================
=            Grid Generator            =
======================================*/


let grid = document.getElementById('hexagonal-grid');
let groupsTable = document.getElementById('groups-table');
var hexCells = [];
var hiddingMaskMap = [];
var gridIsEven = true;

var gridSizeInputs = {
	l_axis: document.querySelector('[name="L-axis"]'),
	m_axis: document.querySelector('[name="M-axis"]'),
	n_axis: document.querySelector('[name="N-axis"]')
}

var gridSize = {
	l_axis: +gridSizeInputs.l_axis.value,
	m_axis: +gridSizeInputs.m_axis.value,
	n_axis: +gridSizeInputs.n_axis.value
}

var fullWidth, fullHeight;

const defineFullSize = ()=>{
	fullWidth = Math.floor(gridSize.l_axis/2 + gridSize.m_axis/2 + gridSize.n_axis-1);
	fullHeight = Math.floor(gridSize.l_axis + gridSize.m_axis - 1);
}


const resetGrid = ()=>{
	[].forEach.call(grid.querySelectorAll('.row'), (elem) => {
		elem.remove();
	});
	[].forEach.call(grid.querySelectorAll('.empty-row'), (elem) => {
		elem.remove();
	});
	emptyGroups = Array.from(colors);
	groups = {};
	grid.classList = 'grid';
}

const renderGrid = (hexCells)=>{
	let rowStr = '<div class="row"></div>';
	let fullGrid = document.createDocumentFragment();

	for (var i = 0; i < hexCells.length; i++) {
		let currentRow = stringToHTML(rowStr).children[0];
		fullGrid.appendChild(currentRow);
		currentRow.id = Math.floor(Math.random() * 10);
		for (let j = 0; j < hexCells[i].length; j++) {
			currentRow.appendChild(hexCells[i][j].getCode());
		}
	}

	resetGrid();

	grid.appendChild(fullGrid);
}

const defineGridScale = ()=>{
	let maxWidth = grid.parentNode.offsetWidth -2;
	let maxWidthPerHex = Math.floor(maxWidth / fullWidth);
	grid.style.fontSize = maxWidthPerHex/2 +'px';
}

const randomizeTheGrid = (probability)=>{
	updateGridSize();
	for (var h = 0; h < fullHeight; h++) {
		for (var w = 0; w < fullWidth; w++) {
			if (hiddingMaskMap[h][w]) {
				continue;
			}
			if (Math.floor(Math.random() * 100) <= probability * 100) {
				hexCells[h][w].activate();
			}
		}
	}
	const renderResults = ()=>{

		let table = document.getElementById('probability');
		let tableBody = table.getElementsByTagName('tbody')[0];
		let tableContentSize = tableBody.rows.length;

		if (table.classList.contains('hidden')) {
			table.classList.remove('hidden');

			for (var i = 0; i < tableContentSize; i++) {
				tableBody.rows[tableBody.rows.length-1].remove();
			}
		}

		var data = [];

		var haveFewConnections = 0;
		var totalNumber = 0;


		for (name in groups) {
			if (groups.hasOwnProperty(name)) {
				totalNumber += window.groups[name].length;
				if (window.groups[name].length > 1) {
					haveFewConnections++;
				}
			}
		}

		data = [
			probability, 
			Object.keys(window.groups).length,
			haveFewConnections,
			totalNumber
			];

		if (tableContentSize > 9) {
			tableBody.deleteRow(0);
		}


		/*----------  output  ----------*/

		let tableRow = tableBody.insertRow(tableBody.length);

		for (var i = data.length - 1; i >= 0; i--) {
			let cellData = tableRow.insertCell(0);
			cellData.classList.add('center');
			cellData.appendChild(stringToHTML('<span>'+data[i]+'</span>').children[0]);
		}


	}
	renderResults();
}


const updateGridSize = ()=>{
	gridSize = {
		l_axis: +gridSizeInputs.l_axis.value,
		m_axis: +gridSizeInputs.m_axis.value,
		n_axis: +gridSizeInputs.n_axis.value
	}

	defineFullSize();
	defineGridScale();

	const invertGrid = ()=>{
		if (!grid.classList.contains('inverted')) {
			grid.prepend(stringToHTML('<span class="empty-row"></span>').children[0]);
			grid.classList.add('inverted')
		} else {
			grid.querySelector('.empty-row').remove();
			grid.classList.remove('inverted');
		}
	}

	const setHiddenHexs = ()=>{

		hiddingMaskMap = [];

		for (var h = 0; h < fullHeight; h++) {
			hiddingMaskMap[h] = hiddingMaskMap[h] || [];
			for (var w = 0; w < fullWidth; w++) {
				hiddingMaskMap[2] = hiddingMaskMap[2] || [];
			}
		}

		/*----------  L-axis  ----------*/
		
		for (var h = 0; h < fullHeight; h++) {
			if (gridSize.l_axis > 1) {
				hiddingMaskMap[h] = hiddingMaskMap[h] || [];
				for (var w = 0; w < fullWidth; w++) {

					let expression = Math.ceil((gridSize.l_axis - h)/2) -1;
					let expressionRefected = Math.floor((gridSize.l_axis - h  + ((fullHeight+1) % 2) )/2) - ((fullHeight+1) % 2);

					if (w < expression) {
						hiddingMaskMap[h][w] = true;
					}
					if (w < expressionRefected ) {
						hiddingMaskMap[fullHeight -1 -h][fullWidth -1 -w] = true;
					}
				}
			}
		}

		/*----------  M-axis  ----------*/
		
		for (var h = 0; h < fullHeight; h++) {
			hiddingMaskMap[h] = hiddingMaskMap[h] || [];
			for (var w = fullWidth - 1; w >= 0; w--) {

				let expression = Math.floor((gridSize.m_axis - h - ((fullHeight+1) % 2))/2) -1 ;
				let expressionRefected = Math.floor((gridSize.m_axis - h +1)/2) -1;

				if (fullWidth-1 - w <= expression) {
					hiddingMaskMap[h][w] = true;
				}
				if (fullWidth-1 - w < expressionRefected) {
						hiddingMaskMap[fullHeight -1 -h][fullWidth -1 -w] = true;
				}
			}
		}



	}
	setHiddenHexs();


	const createHexsArr = ()=>{

		hexCells = [];

		for (var h = 0; h < fullHeight; h++) {
			hexCells[h] = [];
			for (var w = 0; w < fullWidth; w++) {
				hexCells[h][w] = new Hexagon([h, w], {
					isHidden: hiddingMaskMap[h][w] ? hiddingMaskMap[h][w] : false
				});
			}
		}
		renderGrid(hexCells);

		if (gridSize.l_axis % 2 == 0) {
			invertGrid();
		}

	}
	createHexsArr();
	gridIsEven = ((fullHeight+1) % 2);
	console.log('gridIsEven: ', gridIsEven);
}
updateGridSize();




/*=====  End of Grid Generator  ======*/


// }();


/*==============================
=            Events            =
==============================*/

window.addEventListener("load", function(event) {
	[].forEach.call(document.querySelectorAll('.error-msg'), (elem) => {
		elem.remove();
	});
});

document.addEventListener('onChangeGrid', function (e) {

	let tableBody = groupsTable.getElementsByTagName('tbody')[0];

	let tableContentSize = tableBody.rows.length;

	for (var i = 0; i < tableContentSize; i++) {
		tableBody.rows[tableBody.rows.length-1].remove();
	}

	var data = [];

	for (name in groups) {
		if (groups.hasOwnProperty(name)) {
			data.push([name, groups[name].length]);
		}
	}


	for (var i = 0; i < data.length; i++) {
		let tableRow = tableBody.insertRow(tableBody.length);
		let numberCell = tableRow.insertCell(0);
		numberCell.appendChild(stringToHTML('<span>'+data[i][1]+'</span>').children[0]);
		let colorCell = tableRow.insertCell(0);
		colorCell.appendChild(stringToHTML('<span style="color: '+data[i][0]+'">'+data[i][0]+'</span>').children[0]);
	}
});

document.querySelector('#grid-size').addEventListener('submit', function (e) {
	e.preventDefault();
	updateGridSize();
}, false);

document.querySelector('#randomize-grid').addEventListener('submit', function (e) {
	e.preventDefault();
	var probability = e.target.querySelector('[name="probability"]').value;
	randomizeTheGrid(probability);
}, false);

/*=====  End of Events  ======*/
