function isMobile() {
	if (navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i) || navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/BlackBerry/i) || navigator.userAgent.match(/Windows Phone/i)) {
		return true;
	} else {
		return false;
	}
}

if (isMobile()) {
	document.getElementsByTagName('body')[0].classList.add('mobile');
}

/*================================================
=            Trottling and Debouncing            =
================================================*/

var forLastExec,
		delay = 100, // delay between calls
		throttled = false;

function throttling(cb) {
	// only run if we're not throttled
	if (!throttled) {
		// actual callback action
		cb();
		// we're throttled!
		throttled = true;
		// set a timeout to un-throttle
		setTimeout(function() {
			throttled = false;
		}, delay); 
	}
	// last exec on resize end
	clearTimeout(forLastExec);
	forLastExec = setTimeout(cb, delay);
}

/*=====  End of Trottling and Debouncing  ======*/


function stringToHTML(string) {
	var htmlContainer = document.createElement('htmlContainer');
	htmlContainer.innerHTML = string;
	return htmlContainer;
}

let scrollWidth = window.innerWidth - document.documentElement.clientWidth;