

/*=============================
=            Input            =
=============================*/

let updateInputStates = ()=>{}

(()=>{
	var inputsHandler = (wrappers) => [].forEach.call(wrappers, function(label, i) {

		let input = label.querySelector('input') || label.querySelector('textarea');

		input.addEventListener('change', setEmptyState.bind(null, label, input));
		input.addEventListener('focus', () => {label.classList.add('focused')});
		input.addEventListener('blur', () => {label.classList.remove('focused')});

		function setEmptyState(label, input) {

			if (input.value.length >= 1) {

				label.classList.add('not-empty');
				label.classList.remove('empty');

				if (!input.validity.valid) {
					setValidateState(label, input);

					if (!input.classList.contains('set-events')) {
						if ("onpropertychange" in input) {
							input.addEventListener('propertychange', setValidateState.bind(null, label, input));
						} else {
							input.addEventListener('input', setValidateState.bind(null, label, input));
						}
						input.classList.add('set-events')
					}
				}

			} else {
				label.classList.remove('not-empty');
				label.classList.add('empty');
			}
		};

		updateInputStates = ()=>{
			[].forEach.call(wrappers, (wrapper)=>{
				let input = wrapper.querySelector('input') || wrapper.querySelector('textarea');
				setEmptyState(wrapper, input);
			})
		}

		function setValidateState(label, input) {
			if (!input.validity.valid) {
				label.classList.add('invalid');
				label.classList.remove('valid');
				label.querySelector('.input-title').innerHTML = input.dataset.invalidMsg;
			} else {
				label.classList.remove('invalid');
				label.classList.add('valid');
				label.querySelector('.input-title').innerHTML = input.title;
			}
		};

		setEmptyState(label, input);
	});

	var inputs = document.querySelectorAll('label.input-text');
	var textareas = document.querySelectorAll('label.input-textarea');

	inputsHandler([...inputs, ...textareas]);
	// inputsHandler(textareas, 'textarea');
})();

/*=====  End of Input  ======*/



/*==========================================
=            number type inputs            =
==========================================*/


(()=>{
	var numberInputs = document.querySelectorAll('input[type="number"]');

	[].forEach.call(numberInputs, (el, i) => {



		el.addEventListener('focus', () => {
			el.parentElement.classList.add('focused');
		});
		el.addEventListener('blur', () => {
			el.parentElement.classList.remove('focused');
			if (el.hasAttribute('max') && +el.value >= el.getAttribute('max')) {
				return el.value = el.getAttribute('max');
			}
			if (el.hasAttribute('min') && +el.value <= el.getAttribute('min')) {
				return el.value = el.getAttribute('min');
			}
		});

		el.after(stringToHTML('<div class="btns"><div class="inc button">+</div><div class="dec button">&minus;</div></div>').children[0]);

		el.parentElement.querySelector('.inc').addEventListener('click', function(e, el) {
			let step = el.hasAttribute('step') ? +el.getAttribute('step') : 1;
			if (el.hasAttribute('max') && +el.value + step >= el.getAttribute('max')) {
				return el.value = el.getAttribute('max');
			}
			el.value = (+el.value*1000 + step*1000)/1000;
		}.bind(this, null, el));

		el.parentElement.querySelector('.dec').addEventListener('click', function(e, el) {
			let step = el.hasAttribute('step') ? +el.getAttribute('step') : 1;
			if (el.hasAttribute('min') && +el.value - step <= el.getAttribute('min')) {
				return el.value = el.getAttribute('min');
			}
			el.value = (+el.value*1000 - step*1000)/1000;
		}.bind(this, null, el));
	})
})();

/*=====  End of number type inputs  ======*/